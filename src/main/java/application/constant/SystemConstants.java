package application.constant;

public class SystemConstants {

    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_INACTIVE = 0;
}
